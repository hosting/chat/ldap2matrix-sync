import json
import aiofiles
import logging

logger = logging.getLogger(__name__)



class MatrixConfig:
    def __init__(self, config_file: str = "credentials.json"):
        self.config_file = config_file
        self.homeserver: str = ""
        self.access_token: str = ""
        self.user_id: str = ""
        self.device_id: str = ""
        self.basedomain: str = ""

    async def load(self):
        try:
            async with aiofiles.open(self.config_file, "r") as f:
                contents = await f.read()
            config = json.loads(contents)
            self.homeserver = config["homeserver"].rstrip("/")
            self.access_token = config["access_token"]
            self.user_id = config["user_id"]
            self.device_id = config["device_id"]
            self.basedomain = self.user_id.split(':')[1]
            logger.debug("Successfully loaded Matrix configuration")
        except (IOError, json.JSONDecodeError) as e:
            logger.error(f"Failed to load Matrix configuration: {e}")
            raise