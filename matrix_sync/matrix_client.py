import requests
import logging
from typing import List, Dict, Optional, Tuple
from nio import AsyncClient, RoomResolveAliasError, RoomResolveAliasResponse, RoomCreateError
from .matrix_config import MatrixConfig
from nio import RoomKickError

logger = logging.getLogger(__name__)

class RoomAliasException(Exception):
    pass

class UserNotFoundException(Exception):
    pass

class RoomCreationError(Exception):
    pass

class MatrixAdminAccessError(Exception):
    """Raised when the admin user doesn't have the correct permissions"""
    pass

class MatrixClient:
    def __init__(self, config: MatrixConfig):
        self.config = config
        self.client: Optional[AsyncClient] = None

    async def configure(self) -> None:
        try:
            self.client = AsyncClient(self.config.homeserver)
            self.client.access_token = self.config.access_token
            self.client.user_id = self.config.user_id # change this in the config to be admin_user_id
            self.client.device_id = self.config.device_id
            logger.debug("Successfully configured Matrix client")
        except Exception as e:
            logger.error(f"Failed to configure Matrix client: {e}")
            raise

    async def get_room_id_by_room_alias_if_exists(self, room_alias: str) -> Optional[str]:
        try:
            response = await self.client.room_resolve_alias(room_alias)
            if isinstance(response, RoomResolveAliasResponse):
                logger.debug(f"Resolved room alias {room_alias} to {response.room_id}")
                return response.room_id
            elif isinstance(response, RoomResolveAliasError):
                if response.status_code == "M_NOT_FOUND":
                    logger.info(f"Room alias {room_alias} not found")
                    return None
                elif response.status_code == "M_INVALID_PARAM":
                    logger.error(f"Invalid room alias: {room_alias}")
                    raise RoomAliasException(f"Invalid room alias: {room_alias}")
                else:
                    logger.error(f"Error resolving room alias: {response.status_code}, {response.message}")
                    raise RoomAliasException(f"Error resolving room alias: {response.status_code}, {response.message}")
        except Exception as e:
            logger.error(f"Failed to resolve room alias: {e}")
            raise

    async def create_room(self, name: str, alias: str) -> str:
        try:
            # This function logs the following warning twice and also returns Invalid characters in room alias:
            # nio.responses - WARNING - Error validating response: 'room_id' is a required property
            # we only use the response but I don't know how to get rid of the logging, we could report a bug or ask nio
            response = await self.client.room_create(name=name, alias=alias)
            if isinstance(response, RoomCreateError):
                error_message = f"Failed to create room: {response.message}"
                logger.debug(error_message)
                raise RoomCreationError(error_message)
            
            logger.info(f"Created room {name} with alias {alias}, room_id: {response.room_id}")
            return response.room_id
        except RoomCreationError as e:
            raise
        except Exception as e:
            logger.error(f"Failed to create room: {e}")
            raise

    async def get_room_members(self, room_id: str) -> List[str]:
        try:
            url = f"{self.config.homeserver}/_synapse/admin/v1/rooms/{room_id}/members"
            headers = self.get_headers()
            response = requests.get(url, headers=headers)
            response.raise_for_status()
            members = response.json()["members"]
            logger.debug(f"Retrieved {len(members)} members for room {room_id}")
            return members
        except Exception as e:
            logger.error(f"Failed to get room members: {e}")
            raise

    async def add_users_to_room(self, room_id: str, users: List[str]) -> List[str]:
        non_existent_users = []
        for user in users:
            try:
                await self.force_join_user(room_id, user)
                logger.debug(f"Added user {user} to room {room_id}")
            except UserNotFoundException:
                non_existent_users.append(user)
            except MatrixAdminAccessError as e:
                raise
            except Exception as e:
                logger.debug(f"Failed to add user {user} to room {room_id}: {e}")
                raise
        return non_existent_users


    async def kick_users(self, room_id: str, users: List[str]):
        for user in users:
            response = await self.client.room_kick(room_id, user)
            if isinstance(response, RoomKickError):
                logger.debug(f"Response: {response}")
                if response.status_code == "M_FORBIDDEN":
                    logger.debug(f"Failed to kick user {user} from room {room_id}: Insufficient permissions")
                    raise MatrixAdminAccessError(f"Failed to kick user {user}, maybe the admin does not have the required permissions")
                else:
                    raise Exception(f"Failed to kick user {user} from room {room_id}: {response.message}")
            logger.debug(f"Kicked user {user} from room {room_id}")

    async def force_join_user(self, room_id: str, user_id: str):
        url = f"{self.config.homeserver}/_synapse/admin/v1/join/{room_id}"
        payload = {"user_id": user_id}
        headers = self.get_headers()

        try:
            response = requests.post(url, headers=headers, json=payload)
            response.raise_for_status()
            logger.debug(f"User {user_id} was force joined {room_id}")
        except requests.HTTPError as e:
            if e.response.status_code == 404 and e.response.json().get("errcode") == "M_NOT_FOUND":
                raise UserNotFoundException(f"User {user_id} not found")
            else:
                logger.error(f"HTTP error occurred while joining user {user_id} to room {room_id}: {e}")
                # raising this AccessError for now until we implement proper checking of permissions
                raise MatrixAdminAccessError(f"Faild to force join user, maybe the admin user has no permission?")
        except requests.RequestException as e:
            logger.debug(f"Failed to force join user {user_id} to room {room_id}: {e}")
            raise

    def get_headers(self) -> Dict[str, str]:
        return {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.config.access_token}'
        }
    
    async def check_admin_status(self, room_id: str, matrix_members: List[str]) -> Tuple[bool, bool]:
        """
        Check if the bot user is in the room and has admin permissions.
        
        :param room_id: The ID of the room to check
        :param matrix_members: A list of members of Matrix user IDs that are in the room
        :return: A tuple of (is_in_room, is_admin)
        """
        is_in_room = self.config.user_id in matrix_members
        try:
            # TODO find out what permission_type we need here or maybe we just need the power level in the room?
            # TODO don't we also need the power level and check if anyone else is also 100?
            # for now we just assume the user is an admin and let it fail when the force_join function is called
            # we then catch the error and return the MatrixAdminAccessError exception
            is_admin = True # maybe await self.client.has_permission(room_id, "remove_users") ?
            logger.debug(f"Admin status check for room {room_id}: in_room={is_in_room}, is_admin={is_admin}")
            return is_in_room, is_admin
        
        except Exception as e:
            logger.error(f"Failed to check admin status for room {room_id}: {e}")
            return False, False

    async def close(self):
        if self.client:
            await self.client.close()
            logger.info("Matrix client closed")