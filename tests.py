import pytest
from matrix_sync.matrix_client import MatrixClient, RoomAliasException
from matrix_sync.matrix_config import MatrixConfig

@pytest.mark.asyncio
async def test_add_users_to_room_non_existing_user():
    # Load the real MatrixConfig
    config = MatrixConfig()
    await config.load()

    # Create a MatrixClient instance with the real config
    matrix_client = MatrixClient(config)

    # Configure the client
    await matrix_client.configure()

    # Test room alias
    room_alias = "TEST_E020-05.TUchat.Dev"
    full_room_alias = f"#{room_alias}:{config.basedomain}"

    # Get the room ID for the test room
    room_id = await matrix_client.get_room_id_by_room_alias_if_exists(full_room_alias)
    assert room_id is not None, f"Test room {full_room_alias} not found. Please ensure it exists."

    # Test data
    non_existing_user = f"@nonexistent:{config.basedomain}"

    # Call the add_users_to_room method and expect an exception
    with pytest.raises(Exception) as excinfo:
        await matrix_client.add_users_to_room(room_id, [non_existing_user])

    # Check the exception message
    assert "Failed to invite user" in str(excinfo.value), \
        f"Unexpected exception message: {str(excinfo.value)}"

    # Clean up
    await matrix_client.close()