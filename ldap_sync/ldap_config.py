from dotenv import load_dotenv
import os

class LDAPConfig:
    def __init__(self):
        load_dotenv()
        self.ldap_url: str = os.getenv("LDAP_URL", "")
        self.bind_dn: str = os.getenv("BIND_DN", "")
        self.password: str = os.getenv("PASSWORD", "")
        self.base_dn: str = os.getenv("BASE_DN", "")
        self.matrixid_localpart_key: str = os.getenv("MATRIXID_LOCALPART", "")
        self.ldap_field_type: str = os.getenv("LDAP_FIELD_TYPE", "")