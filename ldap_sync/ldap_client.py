import ldap
import logging
from typing import List, Optional
from .ldap_config import LDAPConfig

logger = logging.getLogger(__name__)

# LDAP result codes
LDAP_FILTER_ERROR_CODE = -7

class LDAPConnectionError(Exception):
    """Raised when there's an issue connecting to the LDAP server"""
    pass

class LDAPSearchError(Exception):
    """Raised when there's an issue searching LDAP"""
    pass

class LDAPFilterError(Exception):
    """Raised when the LDAP filter is malformed"""
    pass

class LDAPNoUsersFoundError(Exception):
    """Raised when no users are found for a given LDAP filter"""
    pass

class LDAPClient:
    def __init__(self, config: LDAPConfig):
        self.config = config
        self.connection: Optional[ldap.ldapobject.LDAPObject] = None

    def connect(self):
        try:
            self.connection = ldap.initialize(self.config.ldap_url)
            self.connection.simple_bind_s(self.config.bind_dn, self.config.password)
            logger.info("Successfully connected to LDAP server")
        except ldap.LDAPError as e:
            logger.error(f"Failed to connect to LDAP server: {e}")
            raise LDAPConnectionError(f"Failed to connect to LDAP server: {e}")

    def search_users(self, ldap_filter: str) -> List[str]:
        if not self.connection:
            raise LDAPConnectionError("LDAP connection not established")

        try:
            ldap_results = self.connection.search_s(
                self.config.base_dn, ldap.SCOPE_SUBTREE, ldap_filter, []
            )
            logger.debug(f"LDAP search results: {ldap_results}")

            if not ldap_results:
                logger.debug(f"No results found for LDAP filter: {ldap_filter}")
                raise LDAPNoUsersFoundError(f"No users found for LDAP filter: {ldap_filter}, are you sure you meant to kick out everyone from the room?")

            matrix_users = []
            for ldap_user in ldap_results:
                if not isinstance(ldap_user, tuple) or len(ldap_user) != 2:
                    logger.warning(f"Unexpected LDAP result structure: {ldap_user}")
                    continue

                user_dn, user_attributes = ldap_user
                if not isinstance(user_attributes, dict):
                    logger.warning(f"User attributes are not in expected dictionary format: {user_attributes}")
                    continue

                if self.config.matrixid_localpart_key in user_attributes:
                    mxid_localpart = user_attributes[self.config.matrixid_localpart_key][0].decode("utf-8")
                    if self.config.ldap_field_type == "tuwien":
                        mxid_localpart = f"m{mxid_localpart[5:]}"
                    matrix_users.append(mxid_localpart)
                else:
                    logger.warning(f"User {user_dn} missing {self.config.matrixid_localpart_key}")

            logger.debug(f"Found {len(matrix_users)} matrix users for LDAP filter: {ldap_filter}")
            return matrix_users
        except ldap.FILTER_ERROR as e:
            logger.debug(f"Invalid LDAP filter syntax: {ldap_filter}")
            raise LDAPFilterError(f"Invalid LDAP filter syntax: {ldap_filter}. Error: {str(e)}")
        except ldap.LDAPError as e:
            if self._is_bad_search_filter_error(e):
                logger.debug(f"Bad LDAP search filter: {ldap_filter}")
                raise LDAPFilterError(f"Bad LDAP search filter: {ldap_filter}. Error: {str(e)}")
            else:
                logger.error(f"LDAP search failed: {e}")
                raise LDAPSearchError(f"LDAP search failed: {e}")

    def search_admin_users(self, ldap_filter: str) -> List[str]:
        try:
            ldap_results = self.connection.search_s(
                self.config.base_dn, ldap.SCOPE_SUBTREE, ldap_filter, []
            )
            logger.debug(f"LDAP admin search results: {ldap_results}")

            matrix_users = []
            for ldap_user in ldap_results:
                if not isinstance(ldap_user, tuple) or len(ldap_user) != 2:
                    logger.warning(f"Unexpected LDAP result structure: {ldap_user}")
                    continue

                user_dn, user_attributes = ldap_user
                if not isinstance(user_attributes, dict):
                    logger.warning(f"User attributes are not in expected dictionary format: {user_attributes}")
                    continue

                if self.config.matrixid_localpart_key in user_attributes:
                    mxid_localpart = user_attributes[self.config.matrixid_localpart_key][0].decode("utf-8")
                    if self.config.ldap_field_type == "tuwien":
                        mxid_localpart = f"m{mxid_localpart[5:]}"
                    matrix_users.append(mxid_localpart)
                else:
                    logger.warning(f"User {user_dn} missing {self.config.matrixid_localpart_key}")

            logger.debug(f"Found {len(matrix_users)} admin matrix users for LDAP filter: {ldap_filter}")
            return matrix_users
        except ldap.LDAPError as e:
            logger.error(f"LDAP admin search failed: {e}")
            raise LDAPSearchError(f"LDAP admin search failed: {e}")

    def _is_bad_search_filter_error(self, error: ldap.LDAPError) -> bool:
        """
        Check if the LDAP error is specifically a bad search filter error.
        
        :param error: The LDAP error to check
        :return: True if it's a bad search filter error, False otherwise
        """
        return (isinstance(error.args[0], dict) and 
                error.args[0].get('result') == LDAP_FILTER_ERROR_CODE)

    def close(self):
        if self.connection:
            try:
                self.connection.unbind_s()
                logger.info("LDAP connection closed")
            except ldap.LDAPError as e:
                logger.error(f"Failed to close LDAP connection: {e}")

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()