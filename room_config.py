from typing import Dict, Optional

class ConfigError(Exception):
    """Raised when there's an issue with the room configuration"""
    pass

class RoomConfig:
    def __init__(self, config: Dict):
        self.matrix_room_alias: str = self._get_required(config, 'matrix_room_alias')
        self.ldap_filter: str = self._get_required(config, 'ldap_filter_matrix_users')
        self.auto_kick: bool = self._get_optional(config, 'auto_kick', False)
        self.raw_config: Dict = config

    @staticmethod
    def _get_required(config: Dict, key: str) -> any:
        value = config.get(key)
        if value is None:
            raise ConfigError(f"Missing required configuration key: '{key}'")
        return value

    @staticmethod
    def _get_optional(config: Dict, key: str, default: any) -> any:
        return config.get(key, default)

    def get_optional(self, key: str, default: Optional[any] = None) -> any:
        return self.raw_config.get(key, default)

    @classmethod
    def from_dict(cls, config: Dict) -> 'RoomConfig':
        try:
            return cls(config)
        except ConfigError as e:
            raise ConfigError(f"Invalid room configuration: {str(e)}")

    def __str__(self) -> str:
        return f"RoomConfig(matrix_room_alias={self.matrix_room_alias}, ldap_filter={self.ldap_filter}, auto_kick={self.auto_kick})"