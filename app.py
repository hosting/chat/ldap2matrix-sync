import asyncio
import logging
import yaml
from typing import List
from dotenv import load_dotenv
import os
from ldap_sync.ldap_config import LDAPConfig
from ldap_sync.ldap_client import LDAPClient
from matrix_sync.matrix_config import MatrixConfig
from matrix_sync.matrix_client import MatrixClient
from ldap_matrix_sync import LDAPMatrixSync
from room_config import RoomConfig

# Load environment variables
load_dotenv()

# Set up logging
log_level = os.getenv('LOG_LEVEL', 'INFO').upper()
logging.basicConfig(level=getattr(logging, log_level),
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

class DuplicateAliasError(Exception):
    """Raised when duplicate matrix room aliases are found in the configuration."""
    pass

def load_yaml_config(file_path: str = "matrix_rooms_config.yaml") -> List[RoomConfig]:
    try:
        with open(file_path, "r") as file:
            config_dicts = yaml.safe_load(file)
    except Exception as e:
        logger.debug(f"Failed to load configuration from {file_path}: {e}")
        raise

    room_configs = []
    seen_aliases = set()

    for config_dict in config_dicts:
        room_config = RoomConfig.from_dict(config_dict)
        
        alias = room_config.matrix_room_alias
        if alias in seen_aliases:
            raise DuplicateAliasError(f"Duplicate matrix_room_alias found: '{alias}'.")
        
        seen_aliases.add(alias)
        room_configs.append(room_config)

    logger.debug(f"Loaded configuration for {len(room_configs)} valid rooms from {file_path}")
    return room_configs

async def main():
    try:
        # Load configurations
        ldap_config = LDAPConfig()
        matrix_config = MatrixConfig()
        await matrix_config.load()

        # Initialize matrix client
        matrix_client = MatrixClient(matrix_config)
        await matrix_client.configure()

        # Load YAML configuration
        room_configs = load_yaml_config()
        
        logger.info(f"Loaded Matrix, LDAP, and YAML configuration for {len(room_configs)} rooms")

        # Use 'with' statement for LDAPClient
        with LDAPClient(ldap_config) as ldap_client:
            # Initialize and run sync
            sync = LDAPMatrixSync(ldap_client, matrix_client)
            await sync.sync(room_configs)

    except Exception as e:
        logger.error(f"An error occurred during execution: {e}")
    finally:
        # Cleanup
        if 'matrix_client' in locals():
            await matrix_client.close()

if __name__ == "__main__":
    asyncio.run(main())