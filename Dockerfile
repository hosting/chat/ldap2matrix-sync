FROM python:3.11-slim as build


ENV PIP_DEFAULT_TIMEOUT=100 \
  # Allow statements and log messages to immediately appear
  PYTHONUNBUFFERED=1 \
  # disable a pip version check to reduce run-time & log-spam
  PIP_DISABLE_PIP_VERSION_CHECK=1 \
  # cache is useless in docker image, so disable to reduce image size
  PIP_NO_CACHE_DIR=1

# create working directory and install pip dependencies
WORKDIR /opt/ldap2matrixsync

#explicitly copying files and folders so we dont mistakingly copy wrong stuff
COPY app.py /opt/ldap2matrixsync/
COPY ldap_matrix_sync.py /opt/ldap2matrixsync/ldap_matrix_sync.py
COPY ldap_sync /opt/ldap2matrixsync/ldap_sync
COPY matrix_sync /opt/ldap2matrixsync/matrix_sync
COPY requirements.txt /opt/ldap2matrixsync/requirements.txt
COPY room_config.py /opt/ldap2matrixsync/room_config.py


#Final build stage
FROM python:3.11-slim-buster

WORKDIR /opt/ldap2matrixsync

COPY --from=build /opt/ldap2matrixsync .

#use non-priviled linux user- und groupIDs. Overwrite the UID/GID as needed
ARG UID=1001
ARG GID=1001

RUN set -ex \
  # Create a non-root user
  && addgroup --system --gid "${GID}" appgroup \
  && adduser --system --uid "${UID}" --gid "${GID}" --no-create-home appuser \
  && chown -R "${UID}":"${GID}" /opt/ldap2matrixsync \
  # Upgrade the package index and install security upgrades
  && apt-get update \
  && apt-get upgrade -y \
  #install python dev env needed for some LDAP stuff
  && apt-get install -y python3-dev build-essential libldap2-dev libsasl2-dev ldap-utils curl dnsutils \
  # Install pip dependencies
  && pip install --no-cache-dir -r /opt/ldap2matrixsync/requirements.txt \
  && apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/*

#Set the user to run the application
USER appuser

# run the python script
CMD [ "python3", "app.py"]
