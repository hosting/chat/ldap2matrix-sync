import logging
from typing import List
from ldap_sync.ldap_client import LDAPClient, LDAPFilterError, LDAPNoUsersFoundError
from matrix_sync.matrix_client import MatrixClient, RoomCreationError, MatrixAdminAccessError
from room_config import RoomConfig

logger = logging.getLogger(__name__)

class SyncError(Exception):
    """Raised when there's an issue during the sync process"""
    pass

class LDAPMatrixSync:
    def __init__(self, ldap_client: LDAPClient, matrix_client: MatrixClient):
        self.ldap_client = ldap_client
        self.matrix_client = matrix_client

    async def sync(self, room_configs: List[RoomConfig]):
        try:
            for room_config in room_configs:
                await self.sync_room(room_config)
        except Exception as e:
            raise SyncError(f"Sync process failed: {e}")

    async def sync_room(self, room_config: RoomConfig):
        try:
            logger.info(f"Syncing room {room_config.matrix_room_alias}")

            ldap_members = self.ldap_client.search_users(room_config.ldap_filter)
            room_id = await self.matrix_client.get_room_id_by_room_alias_if_exists(
                f"#{room_config.matrix_room_alias}:{self.matrix_client.config.basedomain}"
            )
            ldap_members_as_matrix_ids = list(map(
                lambda localpart: f"@{localpart}:{self.matrix_client.config.basedomain}",
                ldap_members
            ))
            
            # Get admin users
            ldap_moderator_filter = room_config.get_optional('ldap_moderator')
            admin_users = []
            if ldap_moderator_filter:
                admin_users = self.ldap_client.search_admin_users(ldap_moderator_filter)
                admin_users = list(map(
                    lambda localpart: f"@{localpart}:{self.matrix_client.config.basedomain}",
                    admin_users
                ))

            if not room_id:
                non_existent_users = await self.create_and_populate_room(
                    room_config.matrix_room_alias, ldap_members_as_matrix_ids, admin_users
                )
            else:
                non_existent_users = await self.update_room_members(
                    room_id, ldap_members_as_matrix_ids, room_config.auto_kick, admin_users
                )
            
            if non_existent_users:
                logger.info(f"The following LDAP users for room {room_config.matrix_room_alias} "
                            f"don't have Matrix accounts yet: {', '.join(non_existent_users)}")
                # Here you could implement additional logic, like sending an email to these users or their admin

            logger.info(f"Synced room {room_config.matrix_room_alias}")
            
        except MatrixAdminAccessError as e:
            logger.error(f"For room: {room_config.matrix_room_alias}, {str(e)}")
        except LDAPNoUsersFoundError as e:
            logger.warning(f"Skipping room {room_config.matrix_room_alias}: {str(e)}")
        except LDAPFilterError as e:
            logger.debug(f"LDAP filter error for room {room_config.matrix_room_alias}: {e}")
            raise SyncError(f"Failed to sync room {room_config.matrix_room_alias}: {e}")
            # Here you could implement additional logic, like notifying an admin about the filter issue

        except Exception as e:
            raise SyncError(f"Failed to sync room {room_config.matrix_room_alias}: {e}")

    async def create_and_populate_room(self, room_alias: str, members: List[str], admin_users: List[str]) -> List[str]:
        try:
            room_id = await self.matrix_client.create_room(name=room_alias, alias=room_alias)
            all_users = list(set(members + admin_users))  # Combine and deduplicate members and admin users
            non_existent_users = await self.matrix_client.add_users_to_room(room_id, all_users)
            logger.info(f"Created and populated room {room_alias}")
            logger.info(f"{len(all_users) - len(non_existent_users)} users were added to the room")    
            
            # Set power levels for admin users
            await self.set_admin_power_levels(room_id, admin_users)
            
            return non_existent_users
        except RoomCreationError as e:
            raise SyncError(str(e))
        except Exception as e:
            logger.error(f"Failed to create and populate room {room_alias}: {e}")
            raise SyncError(f"Failed to create and populate room {room_alias}: {e}")

    async def update_room_members(self, room_id: str, ldap_members_as_matrix_ids: List[str], auto_kick: bool, admin_users: List[str]) -> List[str]:
        try:
            matrix_members = await self.matrix_client.get_room_members(room_id)
            await self.validate_room_has_admin_with_correct_permissions(room_id, matrix_members)
            all_ldap_users = list(set(ldap_members_as_matrix_ids + admin_users))  # Combine and deduplicate members and admin users
            users_to_add = set(all_ldap_users) - set(matrix_members)
            non_existent_users = await self.matrix_client.add_users_to_room(room_id, list(users_to_add))
            logger.info(f"{len(users_to_add) - len(non_existent_users)} users were added to the room")

            if auto_kick:
                # remove the admin bot from the members we want to kick
                matrix_members.remove(self.matrix_client.config.user_id)
                users_to_kick = set(matrix_members) - set(all_ldap_users)
                await self.matrix_client.kick_users(room_id, list(users_to_kick))
                logger.info(f"{len(users_to_kick)} were kicked from the room")

            # Set power levels for admin users
            await self.set_admin_power_levels(room_id, admin_users)

            logger.debug(f"Updated members for room {room_id}")
            return non_existent_users
        except MatrixAdminAccessError as e:
            raise
        except Exception as e:
            logger.error(f"Failed to update room members for {room_id}: {e}")
            raise SyncError(f"Failed to update room members for {room_id}: {e}")

    async def set_admin_power_levels(self, room_id: str, admin_users: List[str]):
        try:
            current_power_levels = await self.matrix_client.client.room_get_state_event(room_id, "m.room.power_levels")
            users = current_power_levels.content["users"]
            
            moderators_set = 0
            users_demoted = 0
            for admin_user in admin_users:
                if (admin_user != self.matrix_client.config.user_id and
                    (admin_user not in users or
                     (users[admin_user] != 100 and users[admin_user] != 50))):
                    users[admin_user] = 50
                    moderators_set += 1

            for user, power_level in list(users.items()):
                if (user != self.matrix_client.config.user_id and user not in admin_users and
                     power_level != 100 and power_level != 0):
                        users[user] = 0
                        users_demoted += 1

            await self.matrix_client.client.room_put_state(
                room_id,
                "m.room.power_levels",
                {
                    "users": users,
                    **current_power_levels.content
                }
            )

            logger.info(f"Updated power levels in room {room_id}: "
                        f"{moderators_set} moderators set to 50, "
                        f"{users_demoted} users demoted to 0")
        except Exception as e:
            logger.error(f"Failed to update power levels for room {room_id}: {e}")
            # Don't raise an error here, as this is not critical for room syncing
    
    # where does this function really belong? in which class?
    async def validate_room_has_admin_with_correct_permissions(self, room_id: str, matrix_members: List[str]):
        is_in_room, is_admin = await self.matrix_client.check_admin_status(room_id, matrix_members)
        if not is_in_room:
            raise MatrixAdminAccessError(f"Bot is not in the room")
        elif not is_admin:
            raise MatrixAdminAccessError(f"Bot doesn't have admin permissions for the room")